# Cool stuff (link.tree self-hosted)

Simple website that points audience to my projects.

I keep it simple, modern and minimalistic 🙏

## How to clone it

1) ``git clone https://github.com/satoshinotdead/insta-links``

2) ``cd insta-links`` for editing your stuff (please, double-check the links).

3) Upload to your profile and use!

![image](https://user-images.githubusercontent.com/98671738/224847355-713d8e74-ee86-4fab-8d6d-21f2c73faa5a.png)
